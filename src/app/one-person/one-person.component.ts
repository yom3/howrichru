import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-one-person',
  templateUrl: './one-person.component.html',
  styleUrls: ['./one-person.component.css']
})
export class OnePersonComponent implements OnInit {

  constructor() { }

  @Input() text: string
  @Input() img: string

  ngOnInit(): void {
  }

}
