import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'happy-birthday';

  testText: string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  testImage: string = "../../assets/test.jpg"
  cedricText: string = "Quarante-huit ans, c'est l'âge où l'on commence à ne plus pouvoir espérer vivre le double. Alors profitons-en dès que ce sera possible. Bon anniversaire. Je t'embrasse(rais), Cédric"
  cedricImage: string = "../../assets/cedric.jpg"
  raphText: string = "Joyeux anniversaire Maman!!! ne sois pas trop pressée de rattraper papa, Raph"
  raphImage: string = "../../assets/raph.jpg"
  meText: string = "Je te souhaite un excellent anniversaire, j'espère que tu profiteras bien de ta journée. Je t'aime fort, Emile"
  meImage: string = "../../assets/me.jpg"
  guillaumeText: string = "Joyeux anniversaire semi-confiné à mon ex-binette, dont les blagues salaces après 17h me manquent, sans parler de sa capacité à finir mes phrases ! Grosses bises."
  guillaumeImage: string = "../../assets/guillaume.jpg"
  papaText: string = "Bienvenue en Lozère!"
  papaImage: string = "../../assets/papa.jpg"
  soizicText: string = "Mieux vaut avoir une fois 48 ans que 48 fois un an, nan? Joyeux anniversaire cousine !! Bisou des Âneries. Soizic"
  soizicImage: string = "../../assets/soizic.jpeg"
  elsaText: string = "Joyeux anniversaire grande soeur ! Je te souhaite une 48ième année non confinée avec plein de liberté pour continuer tes passions a fond les ballons comme tu sais si bien le faire! nous t'embrassons tous les 3. See you soon ! 😘 😘 😘"
  elsaImage: string = "../../assets/elsa.jpg"
  dupuyText: string = "Solveig  Tu es belle comme un bijou vermeil,\n Tu rayones comme un soleil,\n Tes lèvres sont rouges comme les groseilles,\n Tu prends 1 an mais tu n'es pas vieille.\n (de toute la famille)\n  Bon anniversaire Solveig"
  dupuyImage: string = "../../assets/dupuy.jpg"
  heleneText: string = "Et où irons nous en 2028? 1990, c'est à Strasbourg que nous avons fait nos premiers pas ensemble. Nous avons tracé la route jusqu'en Chine en 2008. 30 années sont passées, 3 enfants et un compagnon sont arrivés... Dis moi Solveig, où irons nous en 2028? Bon anniversaire soeurette."
  heleneImage: string = "../../assets/helene.jpg"
  armelleText: string = "Bon anniversaire à ma grande fille cherie si pleine de qualités, bienveillante, patiente, énergique, enthousiaste, modeste, etc... Bon, je t'aime, je t'adore sans conditions ❤️, Armelle"
  armelleImage: string = "../../assets/test.jpg"
  perrineImage: string = "../../assets/perrine.jpg"
  perrineText: string = "très bel anniversaire 'petite' cousine Solveig ! Plein de bisous. Perrine"
  constructor() {
  }
}
